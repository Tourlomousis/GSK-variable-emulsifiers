# Workflow for Si-Glass Device Fabrication

**Parts**  
* Si wafer (Part 1)  
* Glass wafer (Part 2)    
* Glass wafer with precut-through holes (Part 3)    
* Aluminum Encasing liquid handling  (Part 4)    

    * Wafer BOM [here]
    (https://docs.google.com/spreadsheets/d/1P9dk9CLwobywBB5aQketpwLNxDrBkNPpEwqkwWF6OyQ/edit#gid=0)  

**Workflow I**  
1. Anodic Bonding of Part 1 on Part 2 at MTL
    - If you want to do piranha cleaning, you need to buy those 2"-4" cassettes
    - Open Coallia and turn machine ON
    - Check for the mounts to be appropriate for anodic bonding
    - 6" mount you can put 3 pairs (Si-glass) of 2" wafers with Si and glas on top of it
    - Apply 50 N normal force
    - Turn ON Heat: 325 deg C - usually it takes 15 min
    - Turn ON Power: 625 Volts
    - Turn OFF Heat
    - Turn ON Cooling - now you can leave and come back after 1.30 - 2 hrs
2. Laser micromachining of emulsifier topography (cut-through vectorized ) on Part 1.  
3. Anodic Bonding of Part 3 and Part1
4. Mount assembly Part 1-2-3 on Part 4.  

**Workflow II**  
1. Laser micromachining of emulsifier topography (engraved rastered) on Part 1.    
2. Anodic bonding of Part 2 on the back side (unpatterned) side of Part 1.  
3. Anodic bonding of Part 3 on the patterned side of Part 1.  
4. Mount assembly of Part 1-2-3 on Part 4.  

## Laser Micromachining of Si Wafers

Here I document parametric studies done with the Oxford Laser aiming to determine
combination of independent process parameters for controlling:  
* Channel Width  
* Channel Height/Depth
* Surface Channel Roughness  

Independent Process parameters:  
* Laser Power  
* Stage Speed  
* Overlay  
* Number of Passes  

### Preparing the toolpath file for Oxford Laser
#### Method 1 (using Photoshop):
1. Save as .dxf file the sketch from Fusion.
2. Open zamzar.com and convert the dxf file to a png file.
2. Open the png file in Adobe Phtoshop.
3. Image Sige - Resampling and Change to more relevant pixel values.
4. Select a known dimension with the selection tool.
5. Open info and write down the pixel value.
6. Compute the px/cm value.
7. Image size,uncheck resampling change to cm and add the resolution value previously computed.
8. Magic Wand Tool - fill in black and white, where appropriate.
9. Save/Export the black and white png file.
10. Upload the image to fabmodules.org and set everything up carefully.
11. fabmodules.org settings:
    * Offset

#### Method 2 (using Will's tool in Fusion):

You can find a G-code routine that Thras created in order to perform parametric
studies [here](https://drive.google.com/drive/folders/1Lq57mPM6ITO5l6v1hsSow73ENKcjRJHL?usp=sharing).